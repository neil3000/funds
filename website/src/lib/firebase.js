import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider, signInWithPopup, fetchSignInMethodsForEmail, OAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
import { fetchAndActivate, getRemoteConfig, isSupported, getValue } from 'firebase/remote-config';
import { writable } from 'svelte/store';
import { modal } from '$lib/modals';
import { toasts } from '$lib/toasts';
import { logs } from '$lib/logs';
// @ts-ignore
import { PUBLIC_FIREBASE_APIKEY, PUBLIC_FIREBASE_AUTHDOMAIN, PUBLIC_FIREBASE_PROJECTID, PUBLIC_FIREBASE_STORAGEBUCKET, PUBLIC_FIREBASE_MESSAGINGSENDERID, PUBLIC_FIREBASE_APPID, PUBLIC_FIREBASE_MEASUREMENTID } from '$env/static/public';

const firebaseConfig = {
	apiKey: PUBLIC_FIREBASE_APIKEY,
	authDomain: PUBLIC_FIREBASE_AUTHDOMAIN,
	projectId: PUBLIC_FIREBASE_PROJECTID,
	storageBucket: PUBLIC_FIREBASE_STORAGEBUCKET,
	messagingSenderId: PUBLIC_FIREBASE_MESSAGINGSENDERID,
	appId: PUBLIC_FIREBASE_APPID,
	measurementId: PUBLIC_FIREBASE_MEASUREMENTID
}

export const app = initializeApp(firebaseConfig, "RahNeil_N3:FirebaseApp:v2");
export const auth = getAuth(app);
export const googleAuthProvider = new GoogleAuthProvider();
export const microsoftAuthProvider = new OAuthProvider('microsoft.com');
export const appleAuthProvider = new OAuthProvider('apple.com');

export const db = getFirestore(app);

isSupported().then((supported) => {
	if (supported) {
		let rc = getRemoteConfig(app);
		fetchAndActivate(rc)
			.then(() => {
				rc_feedback_email.update((v) => getValue(rc, 'feedback_email').asString() || v);
				rc_mainPage_url.update((v) => getValue(rc, 'mainPage_url').asString() || v);
				logs.add({ msg: "Fetched RC values from server" }, "info")
			})
			.catch((err) => {
				console.log(err);
				logs.add(err, "error")
				toasts.feedbackError("31N7BwAzEw@RahNeil_N3:firebase:isSupported:supported:fetchAndActivateRC");
			});
	}
});

export let login = (/** @type {import("@firebase/auth").AuthProvider} */ provider, /** @type {any} */ loginAndLinkModal) => {
	logs.add({ msg: "Opening sign in popup" }, "info")
	signInWithPopup(auth, provider)
		.then((result) => {
			toasts.success('Welcome back ' + result.user.displayName);
			modal.close();
			logs.add({ msg: "Signed in" }, "info")
		})
		.catch((error) => {
			if (error.code === 'auth/account-exists-with-different-credential') {
				logs.add({ msg: "Account already exists with different credentials" }, "info")
				var pendingCred = OAuthProvider.credentialFromError(error);
				var email = error.customData.email;
				fetchSignInMethodsForEmail(auth, email).then(function (methods) {
					if (methods[0] === 'password') {
						/*
						var password = promptUserForPassword(); // TODO: implement promptUserForPassword.
						auth
							.signInWithEmailAndPassword(email, password)
							.then(function (result) {
								// Step 4a.
								return result.user.linkWithCredential(pendingCred);
							})
							.then(function () {
								// Microsoft account successfully linked to the existing Firebase user.
								goToApp();
							});
						return;
						*/
						console.log(error.code)
						console.log(error.message)
						logs.add(error, "error")
						toasts.feedbackError("QursEsQa2C@RahNeil_N3:firebase:login:signInWithPopup:fetchSignInMethodsForEmail:methodPassword");
						modal.close();
					} else if (methods[0] === 'google.com') {
						modal.open(modal, loginAndLinkModal, { providerID: "RahNeil_N3:ProviderID:Xr1pTDZIE4", userCred: pendingCred })
					} else if (methods[0] === 'microsoft.com') {
						modal.open(modal, loginAndLinkModal, { providerID: "RahNeil_N3:ProviderID:ZB8aogoHvU", userCred: pendingCred })
					} else if (methods[0] === 'apple.com') {
						modal.open(modal, loginAndLinkModal, { providerID: "RahNeil_N3:ProviderID:QBK4b9Vv2y", userCred: pendingCred })
					} else {
						console.log(error.code)
						console.log(error.message)
						console.log(methods)
						logs.add({ error: error, methods: methods }, "error")
						toasts.feedbackError("5da8CAQvJD@RahNeil_N3:firebase:login:signInWithPopup:fetchSignInMethodsForEmail:noMethodFound");
						modal.close();
					}
				});
			} else if (error.code === "auth/popup-closed-by-user") {
				toasts.warning('Login popup closed')
				logs.add({ msg: "Popup closed by user" }, "info")
			} else if (error.code === "auth/cancelled-popup-request") {
				logs.add({ msg: "Cancelled popup request" }, "info")
			} else {
				console.log(error.code)
				console.log(error.message)
				logs.add(error, "error")
				toasts.feedbackError("07Lgio5RuM@RahNeil_N3:firebase:login:signInWithPopup:unknownError");
				modal.close();
			}
		});
}

export let rc_feedback_email = writable('your@address.dev');
export let rc_mainPage_url = writable('https://gunivers.net/soutenir');
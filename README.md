**FUNDS (Fun Underway, Need Donations Stat!)**

__STORY:__ The proposed project aims to address the absence of a feature offered by the now-defunct donation platform UTip, which allowed users to view a page detailing the donation cause or how the proceeds would be used before entering their credentials and making a donation. Several of UTip's former users, including us at @Altearn#9268, have had to search for alternatives, many of which do not offer a similar or sufficiently comprehensive feature.

To fill this gap, the project will develop a web application & Android App that allows users to input a URL for a donation platform of their choice. The application will generate shortened URLs and an intermediary page hosted on its own domain that users will see before being redirected to the donation platform. The intermediary page will provide additional information about the donation cause or how the proceeds will be used.

__CONSTRAINTS:__
- The project will have to be made in a few hours only (because i don't have any free time right now)
- Only admins have to be able to create new pages/links


```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
